from kafka import KafkaConsumer

consumer =  KafkaConsumer("jackyld-topic",
                           group_id="jackyld-consumers",
                           bootstrap_servers=["192.168.122.135:9092"])
print "kafka connection established"
for c in consumer:
   print c
