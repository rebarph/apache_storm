from kafka import SimpleProducer, KafkaClient

# To send messages synchronously
kafka = KafkaClient("192.168.122.135:9092")
producer = SimpleProducer(kafka)

# Note that the application is responsible for encoding messages to type str
producer.send_messages("jackyld-topic", "some message")
