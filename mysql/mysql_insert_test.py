from kafka.client import KafkaClient
from kafka.producer import SimpleProducer

import mysql.connector
from mysql.connector import errorcode

try:
  conn = mysql.connector.connect(user='storm',
				password='storm',
				host='192.168.122.115',
                                database='storm')
  print "mysql connection established"
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
cursor = conn.cursor()
test_content = "this is a test"
insert_query = "INSERT INTO test (content) VALUES (%s)"
cursor.execute(insert_query, (test_content,))

# Make sure data is committed to the database
conn.commit()

select_query = "SELECT * FROM test"
cursor.execute(select_query)
for c in cursor:
    print c

cursor.close()
conn.close()

