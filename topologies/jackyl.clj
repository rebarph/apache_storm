(ns wordcount
  (:use     [streamparse.specs])
  (:gen-class))

(defn jackyl [options]
   [
    ;; spout configuration
    {"jackyl-spout" (python-spout-spec
          options
          "spouts.jackyl.JackylSpout"
          ["logs"]
          )
    }
    ;; bolt configuration
    {"streamsplitter-bolt" (python-bolt-spec
          options
          {"jackyl-spout" :shuffle}
          "bolts.streamsplitter.StreamSplitter"
          {"actionitem-stream" ["xmlstring"] 
           "messageitem-stream" ["xmlstring"]}
          :p 2
          )
     "xmltomysql-bolt" (python-bolt-spec
          options
          {"jackyl-spout" :shuffle}
          "bolts.xmltomysql.XmlToMysql"
          []
          :p 4
          )
     "actioncount-bolt" (python-bolt-spec
          options
          {["streamsplitter-bolt" "actionitem-stream"] ["xmlstring"]} ;; subscribe to the car-stream with tuples grouped by car
          "bolts.actioncount.ActionCounter"
          ["user""operation""count"]
          :p 1
          )
     "messagecount-bolt" (python-bolt-spec
          options
          {["streamsplitter-bolt" "messageitem-stream"] ["xmlstring"]} ;; subscribe to the messageitem-stream with tuples grouped by xmlstring
          "bolts.messagecount.MessageCounter"
          ["user""column""count"]
          :p 2
          )
     "actionsave-bolt" (python-bolt-spec
          options
          {"actioncount-bolt" :shuffle}
          "bolts.actionsave.ActionSave"
          []
          :p 4
          )
     "messagesave-bolt" (python-bolt-spec
          options
          {"messagecount-bolt" :shuffle}
          "bolts.messagesave.MessageSave"
          []
          :p 4
          ) 

    }
  ]
)
