from __future__ import absolute_import, print_function, unicode_literals

from streamparse.spout import Spout
from kafka import KafkaConsumer

class JackylSpout(Spout):

    def initialize(self, istormconf, context):
	self.consumer = KafkaConsumer("jackyl",
					group_id="jackyl-consumers",
					bootstrap_servers=["192.168.122.135:9092"])
	
#	self.logspout = []
	
    def next_tuple(self):    
	for logs in self.consumer:
            self.emit([logs.value])
            self.log(logs.value)

    def ack(self, tup_id):
        pass  # if a tuple is processed properly, do nothing

    def fail(self, tup_id):
        pass  # if a tuple fails to process, do nothing
