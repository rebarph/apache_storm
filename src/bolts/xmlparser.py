from __future__ import absolute_import, print_function, unicode_literals
import xml.etree.ElementTree as ET
from streamparse.bolt import Bolt

class XmlParser(Bolt):

      #def initialize(self, conf, ctx):
          #self.message_id = {}

      def process(self, tup):
          self.message_id = {}
          xmlstring = tup.values[0]
          tree = ET.ElementTree(ET.fromstring(xmlstring))
          root = tree.getroot()
          #from_email = root.find('From').text      
          #self.log('From: %s' % (from_email))
          for elem in root.iter():
              self.log('%s: %s' % (elem.tag, elem.text))
              if "ActionItem" in elem.tag:
                 #self.log(elem.text)
                 #sender = elem.text
                 self.emit([xmlstring], stream='actionitem-stream')
              if "MessageID" in elem.tag:
                 self.log(elem.text)
                 msgid = elem.text
          self.message_id[msgid] = sender
          self.log('%s' % self.message_id)
