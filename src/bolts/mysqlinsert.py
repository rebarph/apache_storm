from __future__ import absolute_import, print_function, unicode_literals
import mysql.connector
from mysql.connector import errorcode
#from __future__ import absolute_import, print_function, unicode_literals
from streamparse.bolt import Bolt


class MysqlInsert(Bolt):

    def initialize(self, conf, ctx):
        self.conn = mysql.connector.connect(user='storm',
                               password='storm',
                               host='192.168.122.115',
                               database='storm')
        self.cursor = self.conn.cursor()
    def process(self, tup):
        word = tup.values[0]
        count = tup.values[1]
        insert_query = "INSERT INTO test (content, result) VALUES (%s, %s)"
        self.cursor.execute(insert_query, (word, count))
        self.conn.commit()
