from __future__ import absolute_import, print_function, unicode_literals

from collections import Counter
from streamparse.bolt import Bolt
import xml.etree.cElementTree as ET
import re
class ActionCounter(Bolt):
   
    def initialize(self, conf, ctx):
        self.counts = Counter()
        #self.jackyl_dict = Counter()
        self.test_dict = {}

    def process(self, tup):
        #global test_dict
        xmlstring = tup.values[0]
        tree = ET.ElementTree(ET.fromstring(xmlstring))
        root = tree.getroot()
        self.log("ActionCounter Bolt")
        #for elem in root.iter():
        group_num = 0
        for group in root.findall("ActionItem"):
            group_num += 1
            for elem in group.getchildren():
                #action = "%s %s" % (elem.tag, elem.text)
                #self.counts[action] += 1
                if "From" in elem.tag:
                   raw = elem.text
                   fuser = re.findall(r'<(.*?)>',raw)
                if "UserID" in elem.tag:
                   raw = elem.text
                   uid = re.findall(r'<(.*?)>',raw)
                   self.log("UserID: %s" % uid)
                   if not uid:
                      uid = raw
                      self.log("Raw UserID: %s" % uid)
                   else:
                      uid = uid[0]
                if "Operation" in elem.tag:
                   operation = elem.text
                   if operation == "Jackyl":
                      #action = fuser[0] + '-' + elem.text
                      user_to_use = "From"
                   else:
                      user_to_use = "UserID"
                      #action = uid[0] + '-' + elem.text
                   #self.counts[action] += 1
                
                #if "Subject" in elem.tag:
                #   subject = elem.text   

                #if "Jackyl" in elem.text:
                   #jackyl = elem.text
                   #jackyl_key = uid[0] + "-" + elem.tag
                   #jackyl_key = uid[0] + "-" + "Jackyl"
                   #if not jackyl_key in self.test_dict:
                      #jackyl_key = uid[0] + "-" + "Jackyl"
                   #   self.test_dict[jackyl_key] = 1
                   #else:
                   #   self.test_dict[jackyl_key] += 1
                   #self.log('jackyl_total: %s' % (self.counts[jackyl_key]))
                   #operation =                 

                #if "Reply" in elem.text:
                #   reply = elem.text
                #   if not reply in self.test_dict:
                #      self.test_dict[reply] = 1
                #   else:
                #      self.test_dict[reply] += 1
                #   self.log('reply_total: %s' % (self.counts[reply]))
            if user_to_use == "From":
               username = fuser[0]
               action = username + '-' + operation
            else:
               username = uid
               action = username + '-' + operation
            self.counts[action] += 1    
            self.log('ActionItem counted: %s' % group_num)
            #self.emit([uid[0], operation, self.counts[action]])
            self.emit([username, operation, self.counts[action]])
            self.log('User: %s, Operation: %s, Count:  %d' % (username, operation, self.counts[action]))
        #self.log(self.test_dict)
        #self.log(self.counts.items())
       
        #if user[0] + '_Jackyl' in self.counts[jackyl] and user[0] + '_Reply' in self.counts[jackyl]:
        #   self.log("both elem found!") 
        #if not user[0]+'_reply' in self.jackyl_dict: 
        #   self.jackyl_dict[user[0]+'_reply'] = self.counts[reply] 
        #   self.log('Jackyl Dict %s' % self.jackyl_dict)
        #if not user[0]+'_jackyl' in self.jackyl_dict:
        #   self.jackyl_dict[user[0]+'_jackyl'] = self.counts[jackyl]
        #   self.log('Jackyl Dict %s' % self.jackyl_dict)
        
        #if user[0]+'_jackyl' in self.jackyl_dict and user[0]+'_reply' in self.jackyl_dict:
        #   jackyl_reply_rate = self.jackyl_dict[user[0]+'_jackyl'] / self.jackyl_dict[user[0]+'_reply']
        #   self.log('Jackyl Rate Reply: %s' % (jackyl_reply_rate))
        #else:
        #   self.log('Jackyl Dict %s' % self.jackyl_dict) 
        
        #self.log('Counter jackyl: %s' % self.jackyl_dict['Jackyl']) 
        #self.log('Counter reply: %s' % self.counts['Reply'])  
        #self.log(list(self.counts.elements()))
        #self.log('Count: %s' % self.counts[action])
        self.emit([uid, operation, self.counts[action]])
        self.log('User: %s, Operation: %s, Count:  %d' % (uid, operation, self.counts[action]))
