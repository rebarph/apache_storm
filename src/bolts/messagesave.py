from __future__ import absolute_import, print_function, unicode_literals
import mysql.connector
from mysql.connector import errorcode
#from __future__ import absolute_import, print_function, unicode_literals
from streamparse.bolt import Bolt


class MessageSave(Bolt):

    def initialize(self, conf, ctx):
        self.conn = mysql.connector.connect(user='storm-node',
                               password='storm-cluster-01',
                               host='192.168.122.115',
                               database='aws')
        self.cursor = self.conn.cursor()
    def process(self, tup):
        user = tup.values[0]
        column = tup.values[1]
        count = tup.values[2]
        update_query = "INSERT INTO messageitem (UserID, `%s`) VALUES (\"%s\",%s) ON DUPLICATE KEY UPDATE `%s`=%s" % (column, user, count, column, count)
        self.log(update_query)
        #self.cursor.execute(update_query, (column, user, count, column, count))
        self.cursor.execute(update_query)
        self.conn.commit()
