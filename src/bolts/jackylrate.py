from __future__ import absolute_import, print_function, unicode_literals

from collections import Counter
from streamparse.bolt import Bolt
import xml.etree.cElementTree as ET
import re
class ActionCounter(Bolt):
   
    def initialize(self, conf, ctx):
        self.counts = Counter()
        #self.jackyl_dict = Counter()
        self.test_dict = {}

    def process(self, tup):
        #global test_dict
        xmlstring = tup.values[0]
        tree = ET.ElementTree(ET.fromstring(xmlstring))
        root = tree.getroot()
        self.log("ActionCounter Bolt")
        for elem in root.iter():
            #action = "%s %s" % (elem.tag, elem.text)
            #self.counts[action] += 1
            if "User" in elem.tag:
               raw = elem.text
               user = re.findall(r'<(.*?)>',raw)
            if "Operation" in elem.tag:
               operation = elem.text
               action = user[0] + '_' + elem.text
               self.counts[action] += 1
            if "Jackyl" in elem.text:
               jackyl = elem.text
               if not jackyl in self.test_dict:
                  self.test_dict[jackyl] = 1
               else:
                  self.test_dict[jackyl] += 1
               self.log('jackyl_total: %s' % (self.counts[jackyl]))
            if "Reply" in elem.text:
               reply = elem.text
               if not reply in self.test_dict:
                  self.test_dict[reply] = 1
               else:
                  self.test_dict[reply] += 1
               self.log('reply_total: %s' % (self.counts[reply]))
        self.log(self.test_dict)
               #self.jackyl_dict[user[0]+'_jackyl'] = self.counts[jackyl]
               #self.log('Jackyl Dict %s' % self.jackyl_dict)
               #self.emit([user[0],'jackyl', self.counts[jackyl]], stream="jackyl-rate-stream")
        self.log(self.counts.items())
       
        #if user[0] + '_Jackyl' in self.counts[jackyl] and user[0] + '_Reply' in self.counts[jackyl]:
        #   self.log("both elem found!") 
        #if not user[0]+'_reply' in self.jackyl_dict: 
        #   self.jackyl_dict[user[0]+'_reply'] = self.counts[reply] 
        #   self.log('Jackyl Dict %s' % self.jackyl_dict)
        #if not user[0]+'_jackyl' in self.jackyl_dict:
        #   self.jackyl_dict[user[0]+'_jackyl'] = self.counts[jackyl]
        #   self.log('Jackyl Dict %s' % self.jackyl_dict)
        
        #if user[0]+'_jackyl' in self.jackyl_dict and user[0]+'_reply' in self.jackyl_dict:
        #   jackyl_reply_rate = self.jackyl_dict[user[0]+'_jackyl'] / self.jackyl_dict[user[0]+'_reply']
        #   self.log('Jackyl Rate Reply: %s' % (jackyl_reply_rate))
        #else:
        #   self.log('Jackyl Dict %s' % self.jackyl_dict) 
        
        #self.log('Counter jackyl: %s' % self.jackyl_dict['Jackyl']) 
        #self.log('Counter reply: %s' % self.counts['Reply'])  
        #self.log(list(self.counts.elements()))
        #self.log('Count: %s' % self.counts[action])
        self.emit([user[0], operation, self.counts[action]])
        self.log('User: %s, Operation: %s, Count:  %d' % (user[0], operation, self.counts[action]))
