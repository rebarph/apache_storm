from __future__ import absolute_import, print_function, unicode_literals
import xml.etree.ElementTree as ET
from streamparse.bolt import Bolt

class StreamSplitter(Bolt):

      #def initialize(self, conf, ctx):
          #self.message_id = {}

      def process(self, tup):
          #self.message_id = {}
          #e8s = u.encode('utf-8-sig')
          #xmlstring = (tup.values[0]).encode('utf-8-sig')
          #xmlstring = (tup.values[0]).encode('utf-16')
          #xmlstring = (tup.values[0]).decode('utf-8-sig')
          xmlstring = (tup.values[0]).replace("\ufeff","")
          tree = ET.ElementTree(ET.fromstring(xmlstring))
          root = tree.getroot()
          self.log(root.tag)
          if "ArrayOfActionItem" in root.tag:
                 self.log("ActionItem message")
                 #sender = elem.text
                 self.emit([xmlstring], stream='actionitem-stream')
          if "ArrayOfMessageItem" in root.tag:
                 self.log("MessageItem message")
                 self.emit([xmlstring], stream='messageitem-stream')
          #from_email = root.find('From').text      
          #self.log('From: %s' % (from_email))
          #for elem in root.iter():
              #self.log('%s' % (elem.tag))
              #self.log('%s: %s' % (elem.tag, elem.text))
          #    if "ArrayOfActionItem" in elem.tag:
          #       self.log("ActionItem message")
                 #sender = elem.text
          #       self.emit([xmlstring], stream='actionitem-stream')
          #    if "ArrayOfMessageItem" in elem.tag:
          #       self.log("MessageItem message")
          #       self.emit([xmlstring], stream='messageitem-stream')
          #self.message_id[msgid] = sender
          #self.log('%s' % self.message_id)
