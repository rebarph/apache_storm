from __future__ import absolute_import, print_function, unicode_literals

from collections import Counter
from streamparse.bolt import Bolt
import xml.etree.cElementTree as ET
import re

class MessageCounter(Bolt):

    def initialize(self, conf, ctx):
        self.counts = Counter()
        #self.sent = 0
    def process(self, tup):
        xmlstring = tup.values[0]
        tree = ET.ElementTree(ET.fromstring(xmlstring))
        root = tree.getroot()
        self.log("MessageCounter Bolt")
        #for elem in root.iter():
        group_num = 0
        #sender_sent = 0
        for group in root.findall("MessageItem/Metadata"):
            group_num += 1
            self.log("No. of MessageItem in xml: %s" % group_num)
            sender_sent = 0
            for elem in group.getchildren():
                word = "%s %s" % (elem.tag, elem.text)
                #self.counts[word] += 1
                if elem.tag == "To":
                   raw = elem.text
                   recipients = re.findall(r'<(.*?)>',raw)
                   self.log("Recipients: %s" % (recipients))
                   for receiver in recipients:
                       self.counts[receiver] += 1
                       self.log("Receiver: %s, received: %d" % (receiver, self.counts[receiver]))
                       self.emit([receiver, 'received', self.counts[receiver]])
                       sender_sent += 1
                       self.log("To: %s" % sender_sent)
            
                if elem.tag == "Cc":
                   raw = elem.text
                   self.log("Cc: %s" % (raw))
                   if raw is not None: 
                      recipients = re.findall(r'<(.*?)>',raw)
                      self.log("Recipients: %s" % (recipients))
                      for receiver in recipients:
                          self.counts[receiver] += 1
                          self.log("Receiver: %s, received: %d" % (receiver, self.counts[receiver]))
                          self.emit([receiver, 'received', self.counts[receiver]])
                          sender_sent += 1
                          self.log("Cc: %s" % sender_sent)

                if elem.tag == "Bcc":
                   raw = elem.text
                   self.log("Bcc: %s" % raw)
                   if raw is not None:
                      recipients = re.findall(r'<(.*?)>',raw)
                      self.log("Recipient: %s" % (recipients))
                      for receiver in recipients:
                          self.counts[receiver] += 1
                          self.log("Receiver: %s, received: %d" % (receiver, self.counts[receiver]))
                          self.emit([receiver, 'received', self.counts[receiver]])
                          sender_sent += 1
                          self.log("Bcc: %s" % sender_sent)
 
                if "From" in elem.tag:
                   raw = elem.text
                   sender = re.findall(r'<(.*?)>',raw)
                   self.log("Sender: %s" % (sender)) 
                   self.log("sender sent: %s" % sender_sent)
                   self.counts[sender[0]] += sender_sent
                   self.log('%s: sent: %d' % (sender[0], self.counts[sender[0]]))
               
                   self.emit([sender[0], 'sent', self.counts[sender[0]]])
        #self.emit([user[0], operation, self.counts[action]])
        #self.emit([word, self.counts[word]])
        #for recipient in recipients:
        #    self.log('%s: %s: %d' % (sender, recipient, self.counts[word]))
        #self.log('%s: sent: %d' % (sender, self.sent))
        #self.emit([sender[0], 'sent', self.sent])
        #self.emit([receiver
