from __future__ import absolute_import, print_function, unicode_literals
import xml.etree.ElementTree as ET
import mysql.connector
from mysql.connector import errorcode
from streamparse.bolt import Bolt

class XmlToMysql(Bolt):

      def initialize(self, conf, ctx):
          #self.message_id = {}
          self.conn = mysql.connector.connect(user='storm-node',
                               password='storm-cluster-01',
                               host='192.168.122.115',
                               database='aws')
          self.cursor = self.conn.cursor()

      def process(self, tup):
          #self.message_id = {}
          #xmlstring = (tup.values[0]).encode('utf-8-sig')
          #xmlstring = (tup.values[0]).encode('utf-16')
          #xmlstring = (tup.values[0]).decode('utf-8-sig')
          xmlstring = (tup.values[0]).replace("\ufeff","")
          tree = ET.ElementTree(ET.fromstring(xmlstring))
          # get an iterable and turn it into an iterator
          #context = iter(iterparse("path/to/big.xml", events=("start", "end")))
          #context = iter(iterparse(tree, events=("start", "end")))
          root = tree.getroot()
          self.log(root.tag)
         
          if "ArrayOfActionItem" in root.tag:
             self.log("ActionItem XML message")
             #action_item = root.findall("ActionItem")
             
             #sender = elem.text
             #self.emit([xmlstring], stream='actionitem-stream')
             #for elem in root.iter():
             group_num = 0
             for group in root.findall("ActionItem"):
                 group_num += 1
                 To_Email = ""
                 From_Email = ""
                 UserID = ""
                 for elem in group.getchildren():       
                     #self.log(elem)    
                     if "To" in elem.tag:
                        To_Email = elem.text
                        #self.log(To)
                     #else:
               
                     if "From" in elem.tag:
                        From_Email = elem.text
                     if "UserID" in elem.tag:
                        UserID = elem.text
                     if "MessageID" in elem.tag:
                        MessageID = elem.text
                     if "Operation" in elem.tag:
                        Operation = elem.text
                     if "Subject" in elem.tag:
                        Subject = elem.text
                     if "Timestamp" in elem.tag:
                        Timestamp = elem.text
                     self.log("UserID: %s" % UserID)
                 insert_actions_query = "INSERT INTO actions (`To_Email`, `From_Email`, `UserID`, `MessageID`, `Operation`, `Subject`, `Timestamp`) VALUES (\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\")" % (To_Email, From_Email, UserID, MessageID, Operation, Subject, Timestamp)
                 self.log(insert_actions_query)
                 self.cursor.execute(insert_actions_query)
                 self.conn.commit()
                 self.log("xml actionitem %s converted and saved to mysql" % group_num)
          if "ArrayOfMessageItem" in root.tag:
             self.log("MessageItem XML message")
             #message_item = root.findall("MessageItem")
             #self.log(message_item)
             #for elem in root.iter():
             group_num = 0
             for group in root.findall("MessageItem/Metadata"):
                 group_num += 1
                 self.log('No. of MessageItem: %s' % group_num)
                 #self.log(group)
                 Cc = ""
                 Bcc = ""
                 for elem in group.getchildren():
                     #self.log('%s %s %s' % (elem.tag, elem.attrib, elem.text))
                     if elem.tag == "OriginalMessageID":
                        OriginalMessageID = elem.text
                        #self.log('OriginalMessageID: %s' % OriginalMessageID)
                     if elem.tag == "Subject":
                        Subject = elem.text
                     if elem.tag == "To":
                        To_Email = elem.text
                     if elem.tag == "From":
                        From_Email = elem.text
                     if elem.tag == "Cc":
                        Cc = elem.text
                     if elem.tag == "Bcc":
                        Bcc = elem.text
                     if elem.tag == "ClientSendTime":
                        ClientSendTime = elem.text
                     if elem.tag == "ClientRecieveTime":
                        ClientRecieveTime = elem.text
                     if elem.tag == "MessageID":
                        MessageID = elem.text
                     if elem.tag == "ClientType":
                        ClientType = elem.text
                     if elem.tag == "MessageType":
                        MessageType = elem.text
                     if elem.tag == "ThreadTopic":
                        ThreadTopic = elem.text
                     if elem.tag == "ThreadIndex":
                        ThreadIndex = elem.text
                     if elem.tag == "HasAttachments":
                        HasAttachments = elem.text
                     if elem.tag == "MessageLength":
                        MessageLength = elem.text
                 insert_messages_query = 'INSERT INTO messages (`OriginalMessageID`, `Subject`, `To_Email`, `From_Email`, `Cc`, `Bcc`, `ClientSendTime`, `ClientRecieveTime`, `MessageID`, `ClientType`, `MessageType`, `ThreadTopic`, `ThreadIndex`, `HasAttachments`, `MessageLength`) VALUES (\"%s\",\"%s\", \"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\")' % (OriginalMessageID, Subject, To_Email, From_Email, Cc, Bcc, ClientSendTime, ClientRecieveTime, MessageID, ClientType, MessageType, ThreadTopic, ThreadIndex, HasAttachments, MessageLength)
                 self.log(insert_messages_query)
                 self.cursor.execute(insert_messages_query)
                 self.conn.commit()
                 self.log("xml messageitem converted and saved to mysql")
         
