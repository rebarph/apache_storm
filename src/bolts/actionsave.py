from __future__ import absolute_import, print_function, unicode_literals
import mysql.connector
from mysql.connector import errorcode
#from __future__ import absolute_import, print_function, unicode_literals
from streamparse.bolt import Bolt


class ActionSave(Bolt):

    def initialize(self, conf, ctx):
        self.conn = mysql.connector.connect(user='storm-node',
                               password='storm-cluster-01',
                               host='192.168.122.115',
                               database='aws')
        self.cursor = self.conn.cursor()
    def process(self, tup):
        user = tup.values[0]
        #user = "rine@realestate.ph"
        operation = tup.values[1]
        #operation = "Forward"
        if operation == "To":
           operation = "To_Email"
        if operation == "From":
           operation = "From_Email"
        #subject = tup.values[2]
        count = tup.values[2]
        #count = 1
        update_query = "INSERT INTO actionitem (UserID, `%s`) VALUES (\"%s\",%s) ON DUPLICATE KEY UPDATE `%s`=%s" % (operation, user, count, operation, count)
        #update_query = "INSERT INTO actionitem (UserID, `%s`) VALUES (%s,%s) ON DUPLICATE KEY UPDATE `%s`=%s" % operation
        self.log(update_query)
        #self.cursor.execute(update_query, (operation, user, count, operation, count))
        self.cursor.execute(update_query)
        self.conn.commit()
