import xml.etree.ElementTree as ET
from kafka import KafkaConsumer

consumer =  KafkaConsumer("jackyld-topic",
                           group_id="jackyld-consumers",
                           bootstrap_servers=["192.168.122.135:9092"])
print "kafka connection established"
for c in consumer:
   print c[4]
   #tree = ET.fromstring(c[4])
   tree = ET.ElementTree(ET.fromstring(c[4]))
   root = tree.getroot()
   for elem in root.iter():
       print elem.tag, elem.text

