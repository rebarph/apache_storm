#!/usr/bin/python
import xml.etree.cElementTree as ET
from os import listdir
from os.path import isfile, join
from kafka import SimpleProducer, KafkaClient

# To send messages synchronously
kafka = KafkaClient("192.168.122.135:9092")
producer = SimpleProducer(kafka)

##iterate thru all files in dir
#mypath = "/home/storm/jackyld/sample_logs/"
#onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f))]
#print onlyfiles
  
#for xml_file in onlyfiles:
#    source = mypath + xml_file
#    print source
#    tree = ET.parse(source)
#    root = tree.getroot()
#    msg = ET.tostring(root)
#    print msg
#    producer.send_messages("jackyld-topic", msg)

##test for single source
source = "sample_logs/actions_jackyl.xml"
#source = "sample_logs/actions_reply.xml"
#source = "sample_logs/actions_delete.xml"
#source = "sample_logs/msgs_read.xml"
# Note that the application is responsible for encoding messages to type str
#producer.send_messages("jackyld", "some message")
#xml = open("sample_logs/actions_delete.xml")
#print xml.read()
#msg = xml.read()

tree = ET.parse(source)
root = tree.getroot()
msg = ET.tostring(root)
print msg
producer.send_messages("jackyld-topic", msg)
