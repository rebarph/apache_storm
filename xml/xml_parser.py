import xml.etree.cElementTree as ET
from os import listdir
from os.path import isfile, join
from kafka import SimpleProducer, KafkaClient

kafka = KafkaClient("192.168.122.135:9092")
producer = SimpleProducer(kafka)

mypath = "/home/storm/jackyld/sample_logs/"
onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f))]
print onlyfiles
for xml_file in onlyfiles:
    tree = ET.parse(mypath+xml_file)
    root = tree.getroot()
    #root = tree.iter(tag='Metadata')
    #print root[0]
    for child in root.iter():
        if child.tag == 'MessageItem' or child.tag == 'Metadata':
           pass
        else:
           print child.tag, child.text
           #print child.attrib
           msg = "%s %s" % (child.tag, child.text) 
           producer.send_messages("jackyld-topic", msg)
        
